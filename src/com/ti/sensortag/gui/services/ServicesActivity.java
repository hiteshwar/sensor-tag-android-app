/**************************************************************************************************
  Filename:       ServicesActivity.java
  Revised:        $Date: 2013-08-30 11:44:31 +0200 (fr, 30 aug 2013) $
  Revision:       $Revision: 27454 $

  Copyright 2013 Texas Instruments Incorporated. All rights reserved.
 
  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. 
  The License limits your use, and you acknowledge, that the Software may not be 
  modified, copied or distributed unless used solely and exclusively in conjunction 
  with a Texas Instruments Bluetooth� device. Other than for the foregoing purpose, 
  you may not use, reproduce, copy, prepare derivative works of, modify, distribute, 
  perform, display or sell this Software and/or its documentation for any purpose.
 
  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 
  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com

 **************************************************************************************************/
package com.ti.sensortag.gui.services;

import static com.ti.sensortag.R.drawable.buttonsoffoff;
import static com.ti.sensortag.R.drawable.buttonsoffon;
import static com.ti.sensortag.R.drawable.buttonsonoff;
import static com.ti.sensortag.R.drawable.buttonsonon;
import static com.ti.sensortag.R.drawable.touch;
import static com.ti.sensortag.R.drawable.notouch;
import static com.ti.sensortag.models.Devices.LOST_DEVICE_;
import static com.ti.sensortag.models.Devices.NEW_DEVICE_;
import static com.ti.sensortag.models.Devices.State.CONNECTED;
import static com.ti.sensortag.models.Measurements.PROPERTY_ACCELEROMETER;
import static com.ti.sensortag.models.Measurements.PROPERTY_AMBIENT_TEMPERATURE;
import static com.ti.sensortag.models.Measurements.PROPERTY_GYROSCOPE;
import static com.ti.sensortag.models.Measurements.PROPERTY_HUMIDITY;
import static com.ti.sensortag.models.Measurements.PROPERTY_IR_TEMPERATURE;
import static com.ti.sensortag.models.Measurements.PROPERTY_MAGNETOMETER;
import static com.ti.sensortag.models.Measurements.PROPERTY_SIMPLE_KEYS;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ti.sensortag.R;
import com.ti.sensortag.models.Devices;
import com.ti.sensortag.models.Measurements;
import com.ti.sensortag.models.Point3D;
import com.ti.sensortag.models.SimpleKeysStatus;


public class ServicesActivity extends Activity implements PropertyChangeListener {

  private static final Measurements model = Measurements.INSTANCE;
  private static final char DEGREE_SYM = '\u2103';
  
  //ARV
  private static final String FILENAME = "mysdfile_14.txt";
  String test_1;
  //Button sendbutton;

  DecimalFormat decimal = new DecimalFormat("+0.00;-0.00");

  volatile boolean b = false;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.services_browser);
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    StrictMode.setThreadPolicy(policy);
    test_1 = getIntent().getStringExtra("TEST");
    Toast.makeText(getBaseContext(), "ARV in services:"+test_1, Toast.LENGTH_SHORT).show();
    
   /* sendbutton = (Button) findViewById(R.id.button1);
	sendbutton.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			String sensorid = test_1;
			String sensortype = "temperature";
			BufferedReader br = null;
			 
			try {
	 
				String sCurrentLine;
				
				File ext = Environment.getExternalStorageDirectory();
				File myFile = new File(ext, "mysdfile_25.txt");
	 
				br = new BufferedReader(new FileReader(myFile));
	 
				while ((sCurrentLine = br.readLine()) != null) {
					String[] numberSplit = sCurrentLine.split(":") ; 
					String time = numberSplit[ (numberSplit.length-2) ] ;
					String val = numberSplit[ (numberSplit.length-1) ] ;
					//System.out.println(sCurrentLine);
					HttpResponse httpresponse;
					//int responsecode;
					StatusLine responsecode;
					String strresp;
					
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost("https://rnicu-cloud.appspot.com/sensor/update");
					
					try{
					 List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		             nameValuePairs.add(new BasicNameValuePair("sensorid", sensorid));
		             nameValuePairs.add(new BasicNameValuePair("sensortype", sensortype));
		             nameValuePairs.add(new BasicNameValuePair("time", time));
		             nameValuePairs.add(new BasicNameValuePair("val", val));
		           //  try {
						httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				//		try {
							httpresponse = httpclient.execute(httppost);
							//responsecode = httpresponse.getStatusLine().getStatusCode();
							responsecode = httpresponse.getStatusLine();
							strresp = responsecode.toString();
							Log.d("ARV Response msg from post", httpresponse.toString());
							Log.d("ARV status of post", strresp);
							HttpEntity httpentity = httpresponse.getEntity();
							Log.d("ARV entity string", EntityUtils.toString(httpentity));
						//	Log.d("ARV oly entity", httpentity.toString());
						//	Log.d("ARV Entity response", httpentity.getContent().toString());
							//httpclient.execute(httppost);
							Toast.makeText(getApplicationContext(), "Posted data and returned value:"+ strresp, Toast.LENGTH_LONG).show();
				//		} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
				//			e.printStackTrace();
				//		} catch (IOException e) {
							// TODO Auto-generated catch block
				//			e.printStackTrace();
				//		}
				//	} 
					}catch (ClientProtocolException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
	 
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null)br.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
			
			
		//	Toast.makeText(getApplicationContext(), "Entered on click", Toast.LENGTH_SHORT).show();
			
			
			/*catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
	});*/
    
    
    
    
    
    
    
    getActionBar().setDisplayHomeAsUpEnabled(true);
  }

  @Override
  public void onResume() {
    super.onResume();

    // Setup this view to listen to the model
    // in the traditional MVC pattern.
    model.addPropertyChangeListener(this);

    // Also listen to changes in connection state so
    // we can notify the user with toasts that
    // the device has been disconnected.
    Devices.INSTANCE.addPropertyChangeListener(this);
  }

  @Override
  public void onPause() {
    super.onPause();

    // Stop listening to changes.
    model.removePropertyChangeListener(this);
    Devices.INSTANCE.removePropertyChangeListener(this);
  }

  public void write_data(String sensid,String senstype,String val){
	  long timemilli =  System.currentTimeMillis();
		Log.d("TIMMEE", String.valueOf(timemilli));
		Toast.makeText(getBaseContext(), "IM here", Toast.LENGTH_SHORT).show();
		/*HttpResponse httpresponse;
		//int responsecode;
		StatusLine responsecode;
		String strresp;
		
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("https://rnicu-cloud.appspot.com/sensor/update");
		
		try{
		 List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
       nameValuePairs.add(new BasicNameValuePair("sensorid", sensid));
       nameValuePairs.add(new BasicNameValuePair("sensortype", senstype));
       nameValuePairs.add(new BasicNameValuePair("time", String.valueOf(timemilli)));
       nameValuePairs.add(new BasicNameValuePair("val", val));
     //  try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	//		try {
				httpresponse = httpclient.execute(httppost);
				//responsecode = httpresponse.getStatusLine().getStatusCode();
				responsecode = httpresponse.getStatusLine();
				strresp = responsecode.toString();
				Log.d("ARV Response msg from post", httpresponse.toString());
				Log.d("ARV status of post", strresp);
				HttpEntity httpentity = httpresponse.getEntity();
				Log.d("ARV entity string", EntityUtils.toString(httpentity));
			//	Log.d("ARV oly entity", httpentity.toString());
			//	Log.d("ARV Entity response", httpentity.getContent().toString());
				//httpclient.execute(httppost);
				Toast.makeText(getApplicationContext(), "Posted data and returned value:"+ strresp, Toast.LENGTH_LONG).show();
	//		} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		} catch (IOException e) {
				// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//	} 
		}catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	  
  }
  /**
   * This class listens to changes in the model of sensor values.
   * */
  @Override
  public void propertyChange(final PropertyChangeEvent event) {
    final String property = event.getPropertyName();

    runOnUiThread(new Runnable() {
      public void run() {
        try {        		   
          /*if (property.equals(PROPERTY_ACCELEROMETER)) {
            // A change in accelerometer data has occured.
            Point3D newValue = (Point3D) event.getNewValue();

            String msg = "X: " + decimal.format(newValue.x) + "g" + "\nY: " + decimal.format(newValue.y) + "g" + "\nZ: " + decimal.format(newValue.z) + "g";

   // ARV         ((TextView) findViewById(R.id.accelerometerTxt)).setText(msg);
          } else */ 
          if (property.equals(PROPERTY_AMBIENT_TEMPERATURE)) {
            double newAmbientValue = (Double) event.getNewValue();
            
            final int img;
            TextView textView = (TextView) findViewById(R.id.ambientTemperatureTxt);
            String formattedText = "Touch Not Detected";
            
            if (newAmbientValue == 11.00)
            {
            	formattedText = "Touch Detected";
            	//img = touch;
            
            }	else
            { 
            	
            	//img = notouch;
            }
        

            //((ImageView) findViewById(R.id.touch)).setImageResource(img);
            
           
            
            textView.setText(formattedText);
            
          } else if (property.equals(PROPERTY_IR_TEMPERATURE)) {
            double newIRValue = (Double) event.getNewValue();
          //  float newIRValue_1 = (Float) event.getNewValue();
            TextView textView = (TextView) findViewById(R.id.ir_temperature);
            String value = decimal.format(newIRValue);
           // String formattedText = value + DEGREE_SYM;
            String formattedText = value + DEGREE_SYM;
           float value_chk = (float)(newIRValue);
           String formattedText_1 = String.valueOf(value_chk); 
           String sensorid = test_1;
		   String sensortype = "temperature";
		//	write_data(sensorid,sensortype,value);
			
		//	Toast.makeText(getBaseContext(), "ARV sensid:"+sensorid+" senstype:"+sensortype, Toast.LENGTH_SHORT).show();
			
			long timemilli =  System.currentTimeMillis();
			//Log.d("TIMMEE", String.valueOf(timemilli));
			
			HttpResponse httpresponse;
			//int responsecode;
			StatusLine responsecode;
			String strresp;
			
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("https://rnicu-cloud.appspot.com/sensor/update");
			
			try{
			 List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
             nameValuePairs.add(new BasicNameValuePair("sensorid", sensorid));
             nameValuePairs.add(new BasicNameValuePair("sensortype", sensortype));
             nameValuePairs.add(new BasicNameValuePair("time", String.valueOf(timemilli)));
             nameValuePairs.add(new BasicNameValuePair("val", formattedText_1));
           //  try {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		//		try {
					httpresponse = httpclient.execute(httppost);
					//responsecode = httpresponse.getStatusLine().getStatusCode();
					responsecode = httpresponse.getStatusLine();
					strresp = responsecode.toString();
					Log.d("ARV Response msg from post", httpresponse.toString());
					Log.d("ARV status of post", strresp);
					HttpEntity httpentity = httpresponse.getEntity();
					Log.d("ARV entity string", EntityUtils.toString(httpentity));
				//	Log.d("ARV oly entity", httpentity.toString());
				//	Log.d("ARV Entity response", httpentity.getContent().toString());
					//httpclient.execute(httppost);
					Toast.makeText(getApplicationContext(), "Posted data and returned value:"+ strresp, Toast.LENGTH_LONG).show();
		//		} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		} catch (IOException e) {
					// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		//	} 
			}catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            //ARV
            /*try {
				java.util.Date date = new java.util.Date();
				Timestamp chk = new Timestamp(date.getTime());
				long timemilli =  System.currentTimeMillis();
				String abc = String.valueOf(timemilli);//chk.toString();
				String separator = System.getProperty("line.separator");
				
				File ext = Environment.getExternalStorageDirectory();
				File myFile = new File(ext, "mysdfile_7.txt");
				if(myFile.exists()){
					try {
						FileOutputStream fOut = new FileOutputStream(myFile,true);
					    OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
					    myOutWriter.append(abc);
					    myOutWriter.append("   ");
					    myOutWriter.append(formattedText_1);
					    myOutWriter.append(separator);
					    myOutWriter.flush();
					    myOutWriter.close();
					    fOut.close();
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				else{
					myFile.createNewFile();
				}
				
				
				
				
			//	File myFile = new File("/sdcard/mysdfile.txt");
			//	myFile.createNewFile();
			//	FileOutputStream fOut = new FileOutputStream(myFile);
//ARV				OutputStreamWriter myOutWriter = 
//ARV										new OutputStreamWriter(openFileOutput(FILENAME, Context.MODE_APPEND));//fOut
			//	myOutWriter.append(txtData.getText());
//ARV				myOutWriter.write(abc);
//ARV				myOutWriter.append(separator);
//ARV				myOutWriter.flush();
//ARV				myOutWriter.close();
		//		fOut.close();
				Toast.makeText(getBaseContext(),
						"Done writing SD 'mysdfile.txt'",
						Toast.LENGTH_SHORT).show();
			} catch (Exception e) {
				Toast.makeText(getBaseContext(), e.getMessage(),
						Toast.LENGTH_SHORT).show();
		    } //ARV
*/           
            textView.setText(formattedText);
          }//endif
          
          
          /* else if (property.equals(PROPERTY_HUMIDITY)) {
            double newHumidity = (Double) event.getNewValue();
   // ARV        TextView textView = (TextView) findViewById(R.id.humidityTxt);
            String formattedText = decimal.format(newHumidity) + "%rH";
   //ARV         textView.setText(formattedText);
          } else if (property.equals(PROPERTY_MAGNETOMETER)) {
            Point3D newValue = (Point3D) event.getNewValue();

            String msg = "X: " + decimal.format(newValue.x) + "uT" + "\nY: " + decimal.format(newValue.y) + "uT" + "\nZ: " + decimal.format(newValue.z) + "uT";

   //ARV         ((TextView) findViewById(R.id.magnetometerTxt)).setText(msg);
          } else if (property.equals(PROPERTY_GYROSCOPE)) {
            Point3D newValue = (Point3D) event.getNewValue();

            String msg = "X: " + decimal.format(newValue.x) + "deg/s" + "\nY: " + decimal.format(newValue.y) + "deg/s" + "\nZ: " + decimal.format(newValue.z)
                + "deg/s";

  //ARV          ((TextView) findViewById(R.id.gyroscopeTxt)).setText(msg);
          } else if (property.equals(Measurements.PROPERTY_BAROMETER)) {
            Double newValue = (Double) event.getNewValue();

            String msg = new DecimalFormat("+0.0;-0.0").format(newValue / 100) + " hPa";

 //ARV           ((TextView) findViewById(R.id.barometerTxt)).setText(msg);
          } else if (property.equals(PROPERTY_SIMPLE_KEYS)) {
            SimpleKeysStatus newValue = (SimpleKeysStatus) event.getNewValue();

            final int img;
            switch (newValue) {
            case OFF_OFF:
              img = buttonsoffoff;
              break;
            case OFF_ON:
              img = buttonsoffon;
              break;
            case ON_OFF:
              img = buttonsonoff;
              break;
            case ON_ON:
              img = buttonsonon;
              break;
            default:
              throw new UnsupportedOperationException();
            }

            ((ImageView) findViewById(R.id.buttons)).setImageResource(img);
          }
          else if (property.equals(PROPERTY_SIMPLE_KEYS)) {
              SimpleKeysStatus newValue = (SimpleKeysStatus) event.getNewValue();

              final int img;
              switch (newValue) {
              case OFF_OFF:
                img = buttonsoffoff;
                break;
              case OFF_ON:
                img = buttonsoffon;
                break;
              case ON_OFF:
                img = buttonsonoff;
                break;
              case ON_ON:
                img = buttonsonon;
                break;
              default:
                throw new UnsupportedOperationException();
              }

              ((ImageView) findViewById(R.id.buttons)).setImageResource(img);
            }*/
          else if (property.equals(LOST_DEVICE_ + CONNECTED)) {
            // A device has been disconnected
            // We notify the user with a toast

            int duration = Toast.LENGTH_SHORT;
            String text = "Lost connection";

            Toast.makeText(ServicesActivity.this, text, duration).show();
            finish();
          } else if (property.equals(NEW_DEVICE_ + CONNECTED)) {
            // A device has been disconnected
            // We notify the user with a toast

            int duration = Toast.LENGTH_SHORT;
            String text = "Established connection";

            Toast.makeText(ServicesActivity.this, text, duration).show();
          }
        } catch (NullPointerException e) {
          e.printStackTrace();
          // Could be that the ServicesFragment is no longer visible
          // But we still receive property change events.
          // referring to the views with findViewById will then return a null.
        }
      }
    });
  }
}
